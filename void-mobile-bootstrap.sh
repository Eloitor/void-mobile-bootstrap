#! /bin/bash

. ./lib.sh

usage() {
	cat <<-EOH
	Usage: $PROGNAME [options ...] [command]

	Wrapper script to prepare void-linux images for the pinephone.

	OPTIONS
	 -D device     Install the produced image in device (such as /dev/XXXX)
	 -h            Show this help and exit
	 -V            Show version and exit

  COMMANDS
   build         Build default image
   zap           Clean existing images

	EOH
}

build() {
  sudo ./mkrootfs.sh aarch64
  sudo ./mkplatformfs.sh pinephone void-aarch64-ROOTFS-*.tar.xz 
  sudo ./mkimage.sh void-pinephone-PLATFORMFS-*.tar.xz
  if [ ! -z "$DEVICE" ]; then
    echo "This will install image to device $DEVICE..."
  fi
}

zap() {
  echo "Cleaning existing images..."
  rm *.tar.xz *.img.xz
}

while getopts "d:hV" opt; do
    case $opt in
        d) DEVICE="$OPTARG";;
        V) version; exit 0;;
        h) usage; exit 0 ;;
        *) usage >&2; exit 1 ;;
    esac
done

shift $((OPTIND - 1))
COMMAND="$1"

case "$COMMAND" in
    build) build;;
    zap) zap;;
    *) if [ -n "$COMMAND" ]; then
           echo "Error: Unknown command '$COMMAND'"
           usage
           exit 1
       fi
       ;;
esac
